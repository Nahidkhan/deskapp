/**
 * 
 */
package gnukhata.controllers.reportmodels;

/**
 * @author inu
 *
 */
public class projectstatement {
	/**
	 * @param srNo
	 * @param accountName
	 * @param groupName
	 * @param totalOutgoing
	 * @param totalIncoming
	 */
	
	private String srNo;
	private String accountName;
	private String groupName;
	private String totalOutgoing;
	private String totalIncoming;
	
	public projectstatement(String srNo, String accountName, String groupName,
			String totalOutgoing, String totalIncoming) {
		super();
		this.srNo = srNo;
		this.accountName = accountName;
		this.groupName = groupName;
		this.totalOutgoing = totalOutgoing;
		this.totalIncoming = totalIncoming;
	}
	
	
	
	
	/**
	 * @return the srNo
	 */
	public String getSrNo() {
		return srNo;
	}
	/**
	 * @return the accountName
	 */
	public String getAccountName() {
		return accountName;
	}
	/**
	 * @return the groupName
	 */
	public String getGroupName() {
		return groupName;
	}
	/**
	 * @return the totalOutgoing
	 */
	public String getTotalOutgoing() {
		return totalOutgoing;
	}
	/**
	 * @return the totalIncoming
	 */
	public String getTotalIncoming() {
		return totalIncoming;
	}
	

}
