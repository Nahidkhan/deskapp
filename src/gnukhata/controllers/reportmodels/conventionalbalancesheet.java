/**
 * 
 */
package gnukhata.controllers.reportmodels;

/**
 * @author inu
 *
 */
public class conventionalbalancesheet {
	
	private String CapitalLiabilities;
	private String Amount1;
	private String Amount2;
	private String PropertyAssets;
	private String Amount3;
	private String Amount4;

	/**
	 * @param capitalLiabilities
	 * @param amount1
	 * @param amount2
	 * @param propertyAssets
	 * @param amount3
	 * @param amount
	 */
	public conventionalbalancesheet(String capitalLiabilities, String amount1,
			String amount2, String propertyAssets, String amount3, String amount4) {
		super();
		CapitalLiabilities = capitalLiabilities;
		Amount1 = amount1;
		Amount2 = amount2;
		PropertyAssets = propertyAssets;
		Amount3 = amount3;
		Amount4 = amount4;
	}

	/**
	 * @return the capitalLiabilities
	 */
	public String getCapitalLiabilities() {
		return CapitalLiabilities;
	}

	/**
	 * @return the amount1
	 */
	public String getAmount1() {
		return Amount1;
	}

	/**
	 * @return the amount2
	 */
	public String getAmount2() {
		return Amount2;
	}

	/**
	 * @return the propertyAssets
	 */
	public String getPropertyAssets() {
		return PropertyAssets;
	}

	/**
	 * @return the amount3
	 */
	public String getAmount3() {
		return Amount3;
	}

	/**
	 * @return the amount
	 */
	public String getAmount4() {
		return Amount4;
	}
	
}
